#include "Terminal.h"
#include "NovaVitaLogo.h"
#include "NovaCorConfig.h"

extern "C" {

void kernel_main()
{
	Terminal terminal;
	terminal << logo;
	terminal << Terminal::EOL;

	terminal << Terminal::Good << "Started up Nova Vita (Nova Cor architecture " << NovaCor::Config::Architecture << ")" << Terminal::EOL;
}

}
