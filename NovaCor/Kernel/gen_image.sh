#!/usr/bin/env bash

mkdir -p ./iso/boot/grub
mkdir -p ./iso/System/Kernel/
cp "$1" ./iso/boot/grub/grub.cfg
cp "$2" ./iso/System/Kernel/NovaVita.kernel
"$3" -o ./NovaVita.iso ./iso
